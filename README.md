### resources/application.yml 核心配置文件
配置文件中application.yml中引入mybatis的依赖
### controller包
存放controller类
### service包-impl包
### mapper包
存放mapper接口
### pojo包
存放实体类
### utils包
存放工具类
### BigEventApplication.java启动类
### 用户模块相关接口
- 注册
- 登录
- 获取用户详细信息
- 更新用户基本信息
- 更新用户头像
- 更新用户密码 

Lombok 在编译阶段，为实体类自动生成setter getter toString
pom文件文件中引入依赖 在实体类上添加注解

#### 1.注册
明确需求-阅读接口文档-思路分析-开发-测试
#### 参数校验
Spring Validation
Spring 提供的一个参数校验框架，使用预定义的注解完成参数校验
1. 引入Spring Validation起步依赖
2. 在参数前面添加@Pattern注解
3. 在Controller类上添加@Validated注解
参数校验失败异常处理
@RestControllerAdvice

#### 2.登录
登录认证
在未登录的情况下，可以访问到其他资源
令牌就是一段字符串
- 承载业务数据，减少后续请求查询数据库的次数
- 防篡改，保证信息的合法性和有效性

JWT
- 全程：JSON Web Token(http://jwt.io)
- 定义了一种简洁的、自包含的格式，用于通信双方以json数据格式安全地传输信息
- 组成
- - 第一部分：Header（头），记录令牌类型、签名算法等。例如：{"alg":"HS256","type":"JWT"}
- - 第二部分：Payload（有效载荷），携带一些自定义信息、默认信息等。例如：{"id":"1","username":"Tom"}
- - 第三部分：Signature（签名），防止Token被篡改、确保安全性。将Header、payload，并加入指定密钥，通过指定签名算法计算而来
Base64编码方式

JWT-生成
- JWT校验时使用的签名密钥，必须和生成JWT令牌时使用的密钥是配套的
- 如果JWT令牌解析校验时报错，则说明JWT令牌被篡改或失效了，令牌非法

- 使用拦截器统一验证令牌
- 登录和注册接口需要放行

#### 3.获取用户详细信息

ThreadLocal
提供线程局部变量
- 用来存取数据：set()/get()
- 使用ThreadLocal存储的数据，线程安全
- 用完记得调用remove方法释放，否则可能造成内存泄漏

#### 4.更新用户基本信息

实体类参数校验
- 实体类的成员变量上添加注解
- - @NotNull
- - @NotEmpty
- - @Email
- 接口方法的实体参数上添加@Validated注解

### 3.文章分类
#### 新增文章分类
- 使用validation完成参数校验
- 在service层需要为Category的createUser、createTime、updateTime属性赋值

#### 分组校验
把校验项进行归类分组，在完成不同的功能的时候，校验指定组中的校验项
1. 定义分组
2. 定义校验项时指定归属的分组
3. 校验时指定要校验的分组